package com.globallogic.album;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface AlbumRepository extends CrudRepository<Album, Integer> {
	public List<Album> findByArtistId(Integer artistId);
}
