package com.globallogic.album;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globallogic.artist.Artist;
import com.globallogic.artist.ArtistRepository;

@Service
public class AlbumService {

	private AlbumRepository albumRepository;
	
	@Autowired
	public AlbumService(AlbumRepository albumRepository) {
		super();
		this.albumRepository = albumRepository;
	}

	public List<Album> getAllAlbums(Integer id) {
		List<Album> albums = new ArrayList<>();
		albumRepository.findByArtistId(id).forEach(albums::add);
		return albums;
	}

	public Optional<Album> getAlbum(Integer id) {
		return albumRepository.findById(id);
	}

	public void addAlbum(Album album) {
		albumRepository.save(album);
	}

	public void updateAlbum(Integer id, Album album) {	
		albumRepository.save(album);
	}

	public void deleteAlbum(Integer id) {	
		albumRepository.deleteById(id);
	}

}
