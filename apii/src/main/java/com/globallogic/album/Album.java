package com.globallogic.album;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.globallogic.artist.Artist;

@Entity
@Table(name="music_album")
public class Album {

	@Id
	@Column(name="album_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@Size(max=80)
	@Column(name="album_title")
	private String albumTitle;
	
	@NotNull
	@Size(max=80)
	@Column(name="music_category")
	private String musicCategory;
	
	@NotNull
	@Size(max=80)
	@Column(name="recording_label")
	private String recordingLabel;
	
	@NotNull
	@Column(name="year_released")
	private Integer yearReleased;
	
	@NotNull
	@Column(name="number_of_tracks")
	private Integer numberOfTracks;
	
	@NotNull
	@Size(max=200)
	@Column(name="notes")
	private String notes;
	
	@ManyToOne
	@JoinColumn(name="recording_artist_id")
	private Artist artist;

	public Album() {
		super();
	}

	public Album(Integer id, @NotNull @Size(max = 80) String albumTitle, @NotNull @Size(max = 80) String musicCategory,
			@NotNull @Size(max = 80) String recordingLabel, @NotNull Integer yearReleased,
			@NotNull Integer numberOfTracks, @NotNull @Size(max = 200) String notes, Artist artist) {
		super();
		this.id = id;
		this.albumTitle = albumTitle;
		this.musicCategory = musicCategory;
		this.recordingLabel = recordingLabel;
		this.yearReleased = yearReleased;
		this.numberOfTracks = numberOfTracks;
		this.notes = notes;
		this.artist = artist;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public String getMusicCategory() {
		return musicCategory;
	}

	public void setMusicCategory(String musicCategory) {
		this.musicCategory = musicCategory;
	}

	public String getRecordingLabel() {
		return recordingLabel;
	}

	public void setRecordingLabel(String recordingLabel) {
		this.recordingLabel = recordingLabel;
	}

	public Integer getYearReleased() {
		return yearReleased;
	}

	public void setYearReleased(Integer yearReleased) {
		this.yearReleased = yearReleased;
	}

	public Integer getNumberOfTracks() {
		return numberOfTracks;
	}

	public void setNumberOfTracks(Integer numberOfTracks) {
		this.numberOfTracks = numberOfTracks;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Artist getArtist() {
		return artist;
	}

	public void setArtist(Artist artist) {
		this.artist = artist;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Album [albumTitle=" + albumTitle + "]";
	}
	
	
}
