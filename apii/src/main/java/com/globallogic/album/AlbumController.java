package com.globallogic.album;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.globallogic.artist.Artist;
import com.globallogic.artist.ArtistService;

@RestController
public class AlbumController {

	private AlbumService albumService;

	@Autowired
	public AlbumController(AlbumService albumService) {
		super();
		this.albumService = albumService;
	}
	
	@GetMapping("/artists/{id}/albums")
	public List<Album> getAllAlbums(@PathVariable Integer id) {
		return albumService.getAllAlbums(id);
	}
	
	@GetMapping("/artists/{artistId}/albums/{id}")
	public Optional<Album> getAlbum(@PathVariable Integer id) {
		return albumService.getAlbum(id);
	}
	
	@PostMapping("/artists/{artistId}/albums")
	public void addAlbum(@RequestBody Album album) {
		albumService.addAlbum(album);
	}
	
	@PutMapping("/artists/{artistId}/albums/{id}")
	public void updateAlbum(@RequestBody Album album, @PathVariable Integer id) {
		albumService.updateAlbum(id, album);
	}
	
	@DeleteMapping("artists/{artistId}/albums/{id}")
	public void deleteAlbum(Integer id) {
		albumService.deleteAlbum(id);
	}
}
