package com.globallogic.track;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.globallogic.album.Album;
import com.globallogic.album.AlbumRepository;

@Service
public class TrackService {

private TrackRepository trackRepository;
	
	@Autowired
	public TrackService(TrackRepository trackRepository) {
		super();
		this.trackRepository = trackRepository;
	}
	
	public List<Track> getAllTracks(Integer id) {
		List<Track> tracks = new ArrayList<>();
		trackRepository.findByAlbumId(id).forEach(tracks::add);
		return tracks;

	}

	public Optional<Track> getTrack(Integer id) {
		return trackRepository.findById(id);
	}

	public void addTrack(Track track) {	
		trackRepository.save(track);
	}

	public void updateTrack(Integer id, Track track) {
		trackRepository.save(track);
	}

	public void deleteTrack(Integer id) {
		trackRepository.deleteById(id);
	}

}
