package com.globallogic.track;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.globallogic.album.Album;
import com.globallogic.album.AlbumService;

@RestController
public class TrackController {
	private TrackService trackService;

	@Autowired
	public TrackController(TrackService trackService) {
		super();
		this.trackService = trackService;
	}
	
	@GetMapping("/artists/{artistId}/albums/{id}/tracks")
	public List<Track> getAllTracks(@PathVariable Integer id) {
		return trackService.getAllTracks(id);
	}
	
	@GetMapping("/artists/{artistId}/albums/{albumId}/tracks/{id}")
	public Optional<Track> getTrack(@PathVariable Integer id) {
		return trackService.getTrack(id);
	}
	
	@PostMapping("/artists/{artistId}/albums/{albumId}/tracks")
	public void addTrack(@RequestBody Track track) {
		trackService.addTrack(track);
	}
	
	@PutMapping("/artists/{artistId}/albums/{albumId}/tracks/{id}")
	public void updateTrack(@RequestBody Track track, @PathVariable Integer id) {
		trackService.updateTrack(id, track);
	}
	
	@DeleteMapping("artists/{artistId}/albums/{albumId}/tracks/{id}")
	public void deleteTrack(Integer id) {
		trackService.deleteTrack(id);
	}
}
