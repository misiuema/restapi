package com.globallogic.track;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface TrackRepository extends CrudRepository<Track, Integer> {

	public List<Track> findByAlbumId(Integer albumId);

}
