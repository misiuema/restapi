package com.globallogic.track;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.globallogic.album.Album;

@Entity
@Table(name="album_tracks")
public class Track {
	
	@Id
	@Column(name="track_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@Column(name="track_number")
	private Integer trackNumber;
	
	@NotNull
	@Column(name="track_title")
	@Size(max=80)
	private String trackTitle;
	
	@NotNull
	@Column(name="track_lenght")
	@Size(max=10)
	private String trackLength;

	@ManyToOne
	@JoinColumn(name="album_id")
	private Album album;

	public Track() {
		super();
	}

	public Track(Integer id, @NotNull Integer trackNumber, @NotNull @Size(max = 80) String trackTitle,
			@NotNull String trackLength, Album album) {
		super();
		this.id = id;
		this.trackNumber = trackNumber;
		this.trackTitle = trackTitle;
		this.trackLength = trackLength;
		this.album = album;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTrackNumber() {
		return trackNumber;
	}

	public void setTrackNumber(Integer trackNumber) {
		this.trackNumber = trackNumber;
	}

	public String getTrackTitle() {
		return trackTitle;
	}

	public void setTrackTitle(String trackTitle) {
		this.trackTitle = trackTitle;
	}

	public String getTrackLength() {
		return trackLength;
	}

	public void setTrackLength(String trackLength) {
		this.trackLength = trackLength;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Track other = (Track) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Track [trackTitle=" + trackTitle + "]";
	}
	
	
}
