package com.globallogic.artist;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArtistController {
	
	private ArtistService artistService;

	@Autowired
	public ArtistController(ArtistService artistService) {
		super();
		this.artistService = artistService;
	}
	
	@GetMapping("/artists")
	public List<Artist> getAllArtists() {
		return artistService.getAllArtists();
	}
	
	@GetMapping("/artists/{id}")
	public Optional<Artist> getArtist(@PathVariable Integer id) {
		return artistService.getArtist(id);
	}
	
	@PostMapping("/artists")
	public void addArtist(@RequestBody Artist artist) {
		artistService.addArtist(artist);
	}
	
	@PutMapping("/artists/{id}")
	public void updateArtist(@RequestBody Artist artist, @PathVariable Integer id) {
		artistService.updateArtist(id, artist);
	}
	
	@DeleteMapping("/artists/{id}")
	public void deleteArtist(Integer id) {
		artistService.deleteArtist(id);
	}
	
}
