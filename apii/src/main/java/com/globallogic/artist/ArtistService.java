package com.globallogic.artist;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArtistService {
	
	private ArtistRepository artistRepository;

	@Autowired
	public ArtistService(ArtistRepository artistRepository) {
		super();
		this.artistRepository = artistRepository;
	}
	
	public List<Artist> getAllArtists() {
		List<Artist> artists = new ArrayList<>();
		artistRepository.findAll().forEach(artists::add);
		return artists;
	}

	public Optional<Artist> getArtist(Integer id) {
		return artistRepository.findById(id);
	}

	public void addArtist(Artist artist) {
		artistRepository.save(artist);
		
	}

	public void updateArtist(Integer id, Artist artist) {
		artistRepository.save(artist);
	}

	public void deleteArtist(Integer id) {
		artistRepository.deleteById(id);
		
	}

	
	
}
