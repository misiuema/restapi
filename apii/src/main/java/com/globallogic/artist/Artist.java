package com.globallogic.artist;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.globallogic.album.Album;

@Entity
@Table(name="artists")
public class Artist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="artist_id")
	private Integer id;
	
	@NotNull
	@Size(max=80)
	@Column(name="artist_name")
	private String name;
	
	@NotNull
	@Column(name="age")
	private Integer age;
	
	@NotNull
	@Column(name="nationality")
	private String nationality;
	
	@NotNull
	@Column(name="notes")
	private String notes;

	
	public Artist() {
		super();
	}

	

	public Artist(Integer id, @NotNull @Size(max = 80) String name, @NotNull Integer age, @NotNull String nationality,
			@NotNull String notes) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.nationality = nationality;
		this.notes = notes;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artist other = (Artist) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Artist [name=" + name + "]";
	}


	
}
